/* ЗАДАНИЕ 1
 * Записать в переменную '123', вывести в консоль typeof этой переменной.
 * Преобразовать эту переменную в численный тип при помощи parseInt(), parseFloat(), унарный плюс +
 * После этого повторно вывести в консоль typeof этой переменной.
 * */

let temp = '123.2';

// console.log(hello);
// temp = parseFloat(temp) 
// temp = parseInt(temp)
// temp = +temp;

console.log('исп унарный плюс +  ===>', +temp);
console.log('испл метод parseInt() ===>', parseInt(temp));
console.log('испл метод parseFloat() ===>', parseFloat(temp));
console.log(typeof Number(temp));
