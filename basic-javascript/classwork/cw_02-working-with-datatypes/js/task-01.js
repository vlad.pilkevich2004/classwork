/* TASK 1
* Show up next things in console, using cinsole.log() method:
  - max integer value
  - min integer value
  - not a number
  - max safe integer
  - min safe integer
* */

console.log('Максимальное число', Number.MAX_VALUE);
console.log('Минимальное число', Number.MIN_VALUE);
console.log('NaN', NaN);
console.log('Максимальное безопасное число', Number.MAX_SAFE_INTEGER);
console.log('Минимальное безопасное число', Number.MIX_SAFE_INTEGER);
