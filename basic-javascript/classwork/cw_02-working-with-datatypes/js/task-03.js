/* TASK 3
 * Create 2 variables. Assign true for first one, and false for second one.
 * Execute into console:
 *   - result of comparing 0 and variable with false inside with == operator
 *   - result of comparing 1 and variable with true inside with === operator
 *   - Explain the result
 * */

let booleanTrue = true;
let booleanFalse = false;

console.log('comparing 0 and false => ', 0 == booleanFalse);
console.log('comparing 1 and true => ', 1 === booleanTrue);

// let x = 1;

// x = x + 5;
// x += 5;
// console.log(x);


