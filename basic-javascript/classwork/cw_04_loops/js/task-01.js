/* ЗАДАНИЕ - 1
 * Пользователь должен ввести Имя, Фамилию и свой возраст.
 * В случае если он вводит не соответствующие данные, нужно переспрашивать его, ДО ТЕХ ПОР ПОКА данные не будут введены корректно.
 * */

let userName = prompt(' Name?', 'andrey');
while (userName === '' || userName === null || Number(+userName)) {
	userName = prompt(' Name again?', 'andrey');
}
console.log('userName ---', userName);
let lastName = prompt('LastName?', 'yakovenko');
while (lastName === '' || lastName === null || Number(+lastName)) {
	lastName = prompt(' LastName again?', 'yakovenko');
}
let age = prompt('Age?', 26);
while (age === '' || age === null || isNaN(age) || +age <= 0) {
	age = prompt(' Age again?', 26);
}
