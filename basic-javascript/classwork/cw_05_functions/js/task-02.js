/* ЗАДАНИЕ - 2
 * Написать функцию которая будет принимать два аргумента - число с которого начать отсчет и число до которого нужно досчитать.
 * Под отсчетом имеется в виду последовательный вывод чисел в консоль с увеличением на единицу.
 */

// EXAMPLE 1
// function showRangeNumbers(start, finish) {
// 	if (finish < start) {
// 		for (let i = finish; i < start; i++) {
// 			console.log(i);
// 		}
// 	} else {
// 		for (let i = start; i < finish; i++) {
// 			console.log(i);
// 		}
// 	}
// }

// EXAMPLE 2
// function showRangeNumbers(start, finish) {
// 	if (finish < start) {
// 		let temp = start;
// 		start = finish;
// 		finish = temp;
// 	}
// 	for (let i = start; i < finish; i++) {
// 		console.log(i);
// 	}
// }


// debugger
// showRangeNumbers(15, 10);
