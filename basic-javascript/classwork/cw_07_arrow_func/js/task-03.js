/* TASK - 3
 * Write a function, that will sum up all the arguments passed into it.
 * */

const summ = (...rest) => {
	let sum = 0

	for (let num of rest) {
		if (typeof num === 'number') {
			sum = sum + num
		}
	}
	return sum
}

console.log(summ(11, 2, 3, '4', null, 6, 7, 8, 9, 10))
