/* TASK - 5
* Create Arrow function, that returns maximum value from all of the arguments that was passed.
* The number of arguments can be any. We need to return the biggest one.
* */

const rangeMaxValue = (...allArgs) => Math.max(...allArgs)


console.log(rangeMaxValue(123,214,45,457,578,679,78,997,6,5,764,45,6,4,65,6,4, Infinity, -Infinity));