/* TASK - 1
 * Create a function that does not receive any arguments
 * it will only ask the user what dishes he prefers to eat on breakfast.
 * User can enter only one dish simultaneously, so you need to ask him UNTIL he will enter 'end'.
 * Each dish needs to be placed into an array.
 * Return value: array with all of the dishes.
 * */

// const getBreakfastUser = () => {
// 	let breakfast = []
// 	let dish = prompt('Что будете на завтрак?')

// 	while (dish !== 'end') {
//         breakfast.push(dish)
// 		dish = prompt('Что будете на завтрак?')
// 	}

// 	return breakfast
// }

// console.log('🚀 ==== > breakFast', getBreakfastUser())

