/* TASK - 2
 * Create a function, that will:
 * take an elements from the page with class 'training-list', and text content equals 'list-element 5'.
 * Show this element in console.
 * Replace text content in this element to "<p>Hello</p>" without creating a new HTML element on the page
 * Use array methods to complete the task.
 * */
function changeContent() {
	let listItems = document.querySelectorAll('.training-list')
	// console.log(listItems[0].innerText)
	// console.log(listItems[0].textContent)
	// console.log(listItems[0].outerText)
	// listItems.forEach(currentItem => {
		// console.log(currentItem)
	// 	if (currentItem.textContent === 'list-element 5') {
	// 		currentItem.innerHTML = '<p>Hello</p>'
	// 	}
	// })
	listItems.forEach(function (currentItem) {
		// console.log(currentItem)
		if (currentItem.textContent === 'list-element 5') {
			currentItem.innerHTML = '<h2>Hello</h2>'
			currentItem.firstChild.style.margin = 0
		}
	})
}

changeContent()
